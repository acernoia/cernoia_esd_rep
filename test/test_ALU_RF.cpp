#include <systemc.h>
#include <string>
#include "reg_file.hpp"
#include "ALU.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   //Segnali per il register file
   sc_signal<bool> clock;
   sc_signal<sc_uint<1> > reset, enable;
   sc_signal<sc_uint<3> > target_addr, source1_addr, source2_addr;
   sc_signal<sc_uint<16> > target, source1, source2;
   //Segnali per la ALU
   sc_signal<sc_uint<16> > operando1, operando2, risultato;
   sc_signal<sc_uint<2> > controllo;
   //Componenti da testare
   reg_file RF;
   ALU ALU1;

   SC_CTOR(TestBench) : RF("RF"), ALU1("ALU1")
   {
      SC_THREAD(clock_thread);
      SC_THREAD(stimulus_thread);

      //Collegamento del register file
      RF.CLK(this->clock);
      RF.RST(this->reset);
      RF.WE_RF(this->enable);
      RF.TGT_addr(this->target_addr);
      RF.SRC1_addr(this->source1_addr);
      RF.SRC2_addr(this->source2_addr);
      RF.TGT(this->target);
      RF.SRC1(this->operando1);
      RF.SRC2(this->operando2);

      //Collegamento della ALU
      ALU1.src1(this->operando1);
      ALU1.src2(this->operando2);
      ALU1.alu_out(this->risultato);
      ALU1.func_alu(this->controllo);

      init_test();
   }

   int check()
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != res_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << res_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "CLOCK PERIOD : " << clock_period << " ns" << endl;
        cout << "CLOCK COUNT : " << clock_count/2 << endl;
        cout << "SIMULATION TIME : " << clock_period*clock_count/2 << " ns" << endl;
        cout << "TEST OK" << endl;                 
        return 0;
   }

   private:

   void clock_thread()
   {
     bool value = false;
     while(true) 
     {
            clock.write(value);
            value = !value;
            clock_count++;
            wait(clock_period/2,SC_NS);
      }
   }

   void stimulus_thread()
   {

      //RESET iniziale del register file, tutti i registri inizializzati a 0
      cout << "RESET del register file" << endl;
      reset.write(0);
      wait(clock_period,SC_NS);
      reset.write(1);
      source1_addr.write(indirizzo1);
      source2_addr.write(indirizzo2);
      wait(clock_period,SC_NS);
      cout << "START" << endl << endl;
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        //Scrivi i dati di test nei registri 5 e 6
        target.write(op1_test[i]);
        target_addr.write(indirizzo1);

        wait(clock_period,SC_NS);
        enable.write(1);
        wait(clock_period,SC_NS);
        enable.write(0);
        wait(clock_period,SC_NS);

        target.write(op2_test[i]);
        target_addr.write(indirizzo2);

        wait(clock_period,SC_NS);
        enable.write(1);
        wait(clock_period,SC_NS);
        enable.write(0);
        wait(clock_period,SC_NS);

        cout << "Scritto " << op1_test[i] << " nel registro " << indirizzo1 << endl;
        cout << "Scritto " << op2_test[i] << " nel registro " << indirizzo2 << endl;
        //Leggi i registri 5 e 6 ed esegui l'operazione richiesta


        controllo.write(func_test[i]);
        wait(clock_period,SC_NS);
        cout << "SRC1 = " << operando1.read() << endl;
        cout << "SRC2 = " << operando2.read() << endl;
        dato_letto[i]=risultato.read();
         cout << "Funzione = ";
            switch (func_test[i])
            {
              case 0 : cout << "ADD" << endl; break;
              case 1 : cout << "NAND" << endl; break;
              case 2 : cout << "PASS1" << endl; break;
              case 3 : cout << "EQ?" << endl; break;
             }

        cout << "RISULTATO : " << dato_letto[i] << endl << endl;
     }
   }

   static const unsigned TEST_SIZE = 8;
   unsigned clock_count;
   unsigned clock_period;
   unsigned short target_addr_test, target_test, risultato_test, res1, res2;
   unsigned test_pass;
   unsigned short op1_test[TEST_SIZE];
   unsigned short op2_test[TEST_SIZE];
   unsigned short func_test[TEST_SIZE];
   unsigned short res_test[TEST_SIZE];
   unsigned short dato_letto[TEST_SIZE];
   unsigned short indirizzo1, indirizzo2;
   void init_test()
   {
      clock_count=0;
      clock_period=20;
      test_pass=0;

      indirizzo1=1;
      indirizzo2=7;

      op1_test[0] = 0;
      op1_test[1] = 1;
      op1_test[2] = 0;
      op1_test[3] = 1;
      op1_test[4] = 5;
      op1_test[5] = 239;
      op1_test[6] = 666;
      op1_test[7] = 23;

      op2_test[0] = 0;
      op2_test[1] = 49;
      op2_test[2] = 0;
      op2_test[3] = 1;
      op2_test[4] = 6;
      op2_test[5] = 2;
      op2_test[6] = 666;
      op2_test[7] = 37;

      func_test[0] = 0;
      func_test[1] = 0;
      func_test[2] = 1;
      func_test[3] = 1;
      func_test[4] = 2;
      func_test[5] = 2;
      func_test[6] = 3;
      func_test[7] = 3;


        for (unsigned i=0;i<TEST_SIZE;i++)
        {
          switch (func_test[i])
          {
            case 0 : res_test[i]=op1_test[i]+op2_test[i]; break; //ADD
            case 1 : res_test[i]=~(op1_test[i]&op2_test[i]); break; //NAND
            case 2 : res_test[i]=op1_test[i]; break; //PASS1
            case 3 : if (op1_test[i]==op2_test[i])
	               res_test[i]=1;
                     else
                       res_test[i]=0;
                      break;     //EQ?
          }
        }
   }

};

int sc_main(int argc, char* argv[])
{
  int sim_time = 2000;

  TestBench test("test");

  sc_start(sim_time,SC_NS);

  return test.check();
}

Progetto di Sistemi Elettronici A.A. 2013/2014
Studente: Andrea Cernoia
Matricola: 102195
Progetto: descrizione hardware di architettura RISC

#COMPILAZIONE E TEST:

1) git clone https://acernoia@bitbucket.org/acernoia/cernoia_esd_rep.git

2) cd cernoia_esd_rep

3) git submodule init
   git submodule update

4) mkdir build
   cd build

5) cmake ..
   make

6) test dell'architettura completa:
   ./test_RISC_v2     (architettura con ALU comportamentale)
   oppure
   ./test_RISC_v2_rtl (architettura con ALU RTL)

#NOTE PER IL TEST:

1) Gli eseguibili prendono il codice assembly dal percorso cernoia_esd_rep/assembly/assembly.txt

2) Nella cartella assembly sono presenti alcuni esempi di codice testato.
   Per eseguirli è necessario copiare il codice all'interno del file assembly.txt.

3) Se si vuole scrivere un proprio codice assembly è necessario rispettare la seguente sintassi:
   - un'istruzione per ogni riga di codice
   - non è possibile inserire stringhe o caratteri non previsti dall'instruction set
   - il formato delle istruzioni è: OPCODE<spazio>OPERANDO1<spazio>OPERANDO2<spazio>OPERANDO3

4) L'esecuzione del codice assembly termina quando:
   - viene eseguita l'istruzione HALT
   - viene raggiunto il numero massimo di cicli di clock previsti di default (50)
   - viene raggiunto il numero massimo di cicli di clock richiesto dall'utente (esempio: ./test_RISC_v2 25)

#INSTRUCTION SET QUICK REFERENCE:

ADD Ra Rb Rc
ADDI Ra Rb imm
NAND Ra Rb Rc
LUI Ra imm
SW Ra Rb imm
LW Ra Rb imm
BEQ Ra Rb imm
JALR Ra Rb
NOP
HALT
